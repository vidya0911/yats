from django.conf.urls.defaults import *

from e_details.views import home,emp_page 
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'e_details.views.home', name='home'),
    # url(r'^e_details/', include('e_details.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    ('^home/$', home),
	('^admin/', include(admin.site.urls)),
    ('^emp_page/$',emp_page ),      
)

