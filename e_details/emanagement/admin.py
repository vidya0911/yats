from django.contrib import admin
from emanagement.models import Designation , Business_Unit , Employee , Deleted 

admin.site.register(Designation)
admin.site.register(Business_Unit)
admin.site.register(Employee)
admin.site.register(Deleted)
