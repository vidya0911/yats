from django.db import models

class Designation(models.Model):

    designation=models.CharField(max_length=50)

    def __unicode__(self):
        return self.designation


class Business_Unit(models.Model):
    business_unit = models.CharField(max_length=1500)
  
    def __unicode__(self):
        return self.business_unit 

class Employee(models.Model):
    
    first_name=models.CharField(max_length=20)
    last_name=models.CharField(max_length=20)
    phone_number=models.CharField(max_length=20)
   # photo = models.ImageField('Employee Photo', upload_to ='/media')
    manager=models.ForeignKey(Designation)
    business_unit=models.ForeignKey(Business_Unit)
    joining_date=models.DateField()
    last_modified=models.DateField()
   
    def __unicode__(self):
        return "First Name : {0} , Last_Name : {1}".format(self.first_name,self.last_name)

class Deleted(models.Model):
    first_name=models.CharField(max_length=20)
    last_name=models.CharField(max_length=20)
    phone_number=models.CharField(max_length=20)
    #photo = models.ImageField('Employee Photo', upload_to='/media')
    manager=models.ForeignKey(Designation)
    business_unit=models.ForeignKey(Business_Unit)
    joining_date=models.DateField()
    leaving_date=models.DateField()


    def __unicode__(self):
        return "First Name : {0} , Last_Name : {1}".format(self.first_name,self.last_name)


   



