from django.contrib import admin
from onlinetest.models import Subject , Test , Question , Option ,Correct_Answer,Test_Score

admin.site.register(Subject)
admin.site.register(Test)
admin.site.register(Question)
admin.site.register(Option)
admin.site.register(Correct_Answer)
admin.site.register(Test_Score)
