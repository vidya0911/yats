from django.db import models
from django.contrib import auth
from django.contrib.auth.models import User


#Create your models here.

class Subject(models.Model):
    
    subject=models.CharField(max_length=20)
    
    def __unicode__(self):
        return self.subject


class Test(models.Model):

    test=models.CharField(max_length=50)
    
    subject=models.ForeignKey(Subject)

    def __unicode__(self):
        return self.test


class Question(models.Model):

    question = models.CharField(max_length=1500)
    test=models.ForeignKey(Test)

    def __unicode__(self):
        return self.question 


class Option(models.Model):

    option = models.CharField(max_length=100)
    question = models.ForeignKey(Question)

    def __unicode__(self):
        return self.option


class Correct_Answer(models.Model):

    option = models.ForeignKey(Option)
    question = models.ForeignKey(Question)

    def __unicode__(self):
        return " Question :{0} Option :{1}".format( self.question , self.option)


class Test_Score(models.Model):   # remember models.User

    user  = models.ForeignKey(User)
    question  = models.ForeignKey(Question)
    score = models.IntegerField()
    
    def __unicode__(self):    
        return " User:{0} Question:{1} ".format(self.user, self.question)


class Student(auth.models.User): # remember auth.models.User
    mobile = models.CharField(max_length=20)
    address = models.CharField(max_length=150, blank=True)
    city = models.CharField(max_length=20, blank=True)
    state = models.CharField(max_length=20, blank=True)
    pincode = models.CharField(max_length=10, blank=True)
    country = models.CharField(max_length=20, blank=True)

    def __unicode__(self):
        #return self.user.first_name . self.user.last_name
        return self.username

class Associate(auth.models.User): # remember auth.models.User
    mobile = models.CharField(max_length=20)
    address = models.CharField(max_length=150, blank=True)
    city = models.CharField(max_length=20, blank=True)
    state = models.CharField(max_length=20, blank=True)
    pincode = models.CharField(max_length=10, blank=True)
    country = models.CharField(max_length=20, blank=True)

    def __unicode__(self):
        #return self.user.first_name . self.user.last_name
        return self.username

