from django import forms
from django.forms.util import ErrorList
from django.contrib.auth.models import User
from django.forms import ModelForm, Textarea
from onlinetest.models import Test , Option , Student , Associate
#from yats.views import Take_Test

class ContactForm(forms.Form):
    subject = forms.CharField()
    mail = forms.EmailField(required=False)
    message = forms.CharField()


class QuestionForm(forms.Form):

     def __init__(self, *args, **kwargs):
        test_id = kwargs.pop('test_id', 0)
        super(QuestionForm, self).__init__(*args, **kwargs)
        self.fields['test_id'] = forms.CharField(initial=test_id, widget=forms.widgets.HiddenInput())
        # self.fields['choices'] = forms.ChoiceField(choices=get_my_choices(test_id))
        #widget=forms.widgets.HiddenInput()
        self.fields['choices'] = forms.ChoiceField(choices=get_my_choices(test_id), widget=forms.RadioSelect(), required=True)


def get_my_choices(test_id):
    choice_list = Option.objects.filter(test_id=test_id)
   # TODO convert this into a proper format look at dynamic workflows code
    return choice_list



class StudentSignupForm(ModelForm):
    error_messages = {
        'duplicate_username': "A user with that username already exists.",
        'password_mismatch': "The two password fields didn't match.",
    }
    username = forms.RegexField(label="Username", max_length=30,
        regex=r'^[\w.@_]+$',
        help_text="Required. 20 characters or fewer. Letters, digits and "
                      "@/_/. only.",
        error_messages={
            'invalid': "This value may contain only letters, numbers and "
                         "@/_/. characters."})
    password1 = forms.CharField(label="Password",
        widget=forms.PasswordInput)
#    password2 = forms.CharField(label="Password confirmation",
#        widget=forms.PasswordInput,
#        help_text="Enter the same password as above, for verification.")

    class Meta:
        
        model = Student
        fields = ("username", "password1",
                  "first_name", "last_name",
                  "email", "mobile",
                  "address", "city", "state" , "pincode" , "country")
        widgets = {
            'address': Textarea(attrs={'cols': 40, 'rows': 4}),
        }

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

#    def clean_password2(self):
#        password1 = self.cleaned_data.get("password1", "")
#        password2 = self.cleaned_data["password2"]
#        if password1 != password2:
#            raise forms.ValidationError(
#                self.error_messages['password_mismatch'])
#        return password2

    def save(self, commit=True):
        student = super(StudentSignupForm, self).save(commit=False)
        student.set_password(self.cleaned_data["password1"])
        if commit:
            student.save()
        return student

    def as_mytable(self):
        return self._html_output(
            normal_row=u'<tr%(html_class_attr)s><td>%(label)s</td><td>%(errors)s%(field)s%(help_text)s</td></tr>',
            error_row=u'<tr><td colspan="2">%s</td></tr>',
            row_ender=u'</td></tr>',
            help_text_html=u'<br /><span class="helptext">%s</span>',
            errors_on_separate_row=False)



class AssociateSignupForm(ModelForm):
    error_messages = {
        'duplicate_username': "A user with that username already exists.",
        'password_mismatch': "The two password fields didn't match.",
    }
    username = forms.RegexField(label="Username ", max_length=30,
        regex=r'^[\w.@_]+$',
        help_text="Required. 20 characters or fewer. Letters, digits and "
                      "@/_/. only.",
        error_messages={
            'invalid': "This value may contain only letters, numbers and "
                         "@/_/. characters."})

    password1 = forms.CharField(label="Password",
        widget=forms.PasswordInput)
#    password2 = forms.CharField(label="Password confirmation",
#        widget=forms.PasswordInput,
#        help_text="Enter the same password as above, for verification.")
     
    

    class Meta:
        model = Associate
        fields = ("username", "password1",
                  "first_name", "last_name",
                  "email", "mobile",
                  "address", "city", "state", "pincode", "country")
        widgets = {
            'address': Textarea(attrs={'cols': 40, 'rows': 4}),
        }

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

#    def clean_password2(self):
#        password1 = self.cleaned_data.get("password1", "")
#        password2 = self.cleaned_data["password2"]
#        if password1 != password2:
#            raise forms.ValidationError(
#                self.error_messages['password_mismatch'])
#        return password2

    def save(self):
        associate = super(AssociateSignupForm, self).save(commit=False)
        associate.set_password(self.cleaned_data["password1"]) 
        associate.is_staff=True
        associate.is_superuser=True
        associate.save()
        return associate

    def as_mytable(self):
        return self._html_output(
            normal_row=u'<tr%(html_class_attr)s><td>%(label)s</td><td>%(errors)s%(field)s%(help_text)s</td></tr>',
            error_row=u'<tr><td colspan="2">%s</td></tr>',
            row_ender=u'</td></tr>',
            help_text_html=u'<br /><span class="helptext">%s</span>',
            errors_on_separate_row=False)

