from django.conf.urls import patterns, include, url
from yats.views import home, about_us, our_team, our_plan , contact , student_sign_up ,  associate_sign_up , choose_subject,choose_test ,take_test ,check_answer   ,logout_view, thanks_associates , mytestscore ,show_score,thanks_for_test
from django.contrib import admin
from django.contrib.auth.views import login
from django.contrib.auth.decorators import login_required
from forms import StudentSignupForm , AssociateSignupForm
admin.autodiscover()

urlpatterns = patterns('',
        ('^$', home),
        ('^home/$', home),
        ('^associate_login/', include(admin.site.urls)),  # here we need to understand the most basic concept of Django - here Django on request of /home will look
        ('^about_us/$', about_us),  # for home function in the views.py , also we should not forget to import those function in the views.py
        ('^our_team/$', our_team),
        ('^our_plan/$', our_plan),      
        ('^contact_us/$', contact),
    
        ('^login/$', login, {'template_name': 'login.html'}),
        ('^student_sign_up/$', student_sign_up, {'template_name': 'Student_Sign_Up.html', 'redirect_to': '/login/', 'signup_form': StudentSignupForm}),
        
        ('^logout/$',logout_view),
        ('^associate_sign_up/$', associate_sign_up, {'template_name': 'Associate_Sign_Up.html', 'redirect_to': '/thanks_associates/', 'signup_form': AssociateSignupForm}),
        ('^choose_subject/$', choose_subject),
        ('^choose_subject/(\d{1,2})/$',choose_test),
        ('^choose_subject/(\d{1,2})/(\d{1,2})/$',take_test),

        
        ('^check_answer/$', check_answer),
        ('^mytestscore/$',mytestscore),
        ('^show_score/$',show_score),
        ('^thanks_associates/$',thanks_associates),
        ('^thanks/$',thanks_associates),
        ('^thanks_for_test/$',thanks_for_test),
        
        )
# see the $ sign at the end of the say-hello if there will not be $ sign then it will take any usl preceded by say-hello i.e: say-hello/vidya/sagar/
# is a valid as if there will be no $ at the end

