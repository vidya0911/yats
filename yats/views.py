from django.core.mail import send_mail
from django.http import HttpResponse , HttpRequest
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib import auth
from django.contrib.auth.models import User 
from django.db.models import Sum
from django.contrib.auth.forms import AuthenticationForm
#from django.template.loader import get_template
#from django.template import Context
from django.shortcuts import render_to_response
#from django.shortcuts import render
from yats.forms import ContactForm, QuestionForm ,StudentSignupForm ,AssociateSignupForm# import form object as it is need in this view
from onlinetest.models import Test, Option , Correct_Answer , Test_Score , Subject , Option ,Question
 # Import Test model in  defined in onlinetest(app).models i.e: we need it in this view
from django.template import RequestContext
from django.http import QueryDict
  

  # static ages

def home(request):
    return render(request,'home.html')


def about_us(request):
    return render(request,'about.html')


def our_team(request):

   return render_to_response('team.html',
        context_instance=RequestContext(request))

def thanks_for_test(request):
    return render(request,'thanks_for_test.html')


                            


def our_plan(request):
    return render(request,'plan.html')

def contact(request):

    if request.method == 'POST':
        form = ContactForm(request.POST)  # here we are trying to create a object of ContactForm named form i.e: form will have those 3 class field
        if form.is_valid():
            cd = form.cleaned_data
            send_mail(
                cd['subject'],
                cd['message'],
                cd.get('email', 'vidya@p3infotech.in'),
                ['vidya@p3infotech.in'],
            )
            return HttpResponseRedirect('/thanks/')
    else:
        form = ContactForm()
    return render(request, 'contact.html', {'form': form})



# sign up and login 

def student_sign_up(request, template_name, signup_form, redirect_to):
    if (request.method != 'POST'):
        return render_to_response(
            template_name,
            {'form': signup_form()},
            context_instance=RequestContext(request))

    form = signup_form(request.POST)
    if not form.is_valid():
        return render_to_response(
            template_name,
            {'form': form},
            context_instance=RequestContext(request))

    form.save()
    data = form.cleaned_data
    user = auth.authenticate(username=data['username'],
                             password=data['password1'])
    assert(user != None)
    auth.login(request, user)

    # Redirect to a success page.
    return HttpResponseRedirect(redirect_to)


def associate_sign_up(request, template_name, signup_form, redirect_to):
    if (request.method != 'POST'):
        return render_to_response(
            template_name,
            {'form': signup_form()},
            context_instance=RequestContext(request))

    form = signup_form(request.POST)
    if not form.is_valid():
        return render_to_response(
            template_name,
            {'form': form},
            context_instance=RequestContext(request))

    form.save()
    # Redirect to a success page.
    return HttpResponseRedirect('/thanks/')

def thanks_associates(request):

    return render_to_response('thanks_associates.html')


def logout_view(request):
   
    auth.logout(request)
    # Redirect to a success page.
    return HttpResponseRedirect('/')



# generate test and take test 


def choose_subject(request):   # here we will select a Subject to take test

    if request.user.is_authenticated():
        subject = Subject.objects.filter()
        return render_to_response('choose_subject.html',
                              {'subject': subject},context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/login/?next=%s' % request.path)                 


def choose_test(request , subject_id): # it will show the test available with the subject choosen
    


    test = Test.objects.filter(subject_id=subject_id)
    return render_to_response('choose_test.html',
                              {'test': test})
                              

def take_test(request ,subject_id, test_id): # now the test begin , if the user is logged in he can take the test
    
    if request.user.is_authenticated():       
        return check_test_repeat(request ,subject_id, test_id)
        
    else:
        return HttpResponseRedirect('/login/')



def check_test_repeat(request ,subject_id, test_id): # now the test begin , if the user is logged in he can take the test
    
    quest_set = Question.objects.filter(test_id=test_id)
    question_no=0
    for question in quest_set:
        q_id=question.id
        quest_test_score_no=Test_Score.objects.filter(question_id=q_id , user_id=request.user.id).count()
        if quest_test_score_no == 0:
            return display_question(request , test_id ,question_no)
        else:
            return render(request,'test_repeat.html')


    #return display_question(request , test_id ,question_no) # this will display the question available with the choosen test
   

def check_answer(request):       # is called from take_test.html as a action of form generated in take_test.html 
    
    if request.method == 'POST':
        question_id = request.POST['question_id']
        question_no= int(request.POST['question'])
        test_id=request.POST['test_id']
       
        try :
            user_option=request.POST['option_clicked']
        
        except KeyError:
            question_no=question_no-1
            return display_question(request , test_id ,question_no) 
            # this will display the question available with the choosen test
    
        user_answer = Option.objects.get(question_id=question_id , option =user_option)
       
        user_answer_id=user_answer.id

        correct_answer=Correct_Answer.objects.get(question_id= question_id)
        correct_answer_id=correct_answer.option_id
    
        if correct_answer_id == user_answer_id:
            
            score=10 
            return myscore(request , question_id , test_id,score , question_no)
        else:    
            score=-3
            return myscore(request , question_id , test_id,score,question_no)
        
    else:

        return HttpResponseRedirect('/about/')  # only for testing


 # after test section score release

def myscore(request, question_id , test_id,score,question_no):  # this view save the score in database

    if request.user.is_authenticated():
        test_score = Test_Score()
        test_score.user = User.objects.get(id=request.user.id)
        test_score.question_id=question_id
        test_score.score=score
        test_score.save()
        return  display_question(request , test_id ,question_no)
        #return render_to_response('score_card.html',{'test':test_score.question_id , 'score':test_score.score , 'user':test_score.user})
                 
    else:
        return render_to_response('about.html')

def display_question(request , test_id , question_no ):
     
    questions=Question.objects.filter(test_id=test_id)
    total_question = questions.count()
    if question_no < total_question: 
        question=questions[question_no].question
        question_id=questions[question_no].id
        option=Option.objects.filter(question_id=question_id)
        question_no=question_no+1
        return render_to_response('take_test.html',
                              {'question': question , 'option':option ,'question_id':question_id ,'test_id':test_id ,'question_no':question_no})
    else:
      #  render_to_response('thanks_for_test.html')
      return HttpResponseRedirect('/thanks_for_test/')




def mytestscore(request):

    if request.user.is_authenticated():
        username = User.objects.get(id=request.user.id)
        mytestscore=Test_Score.objects.filter(user=username) 
        test_taken_detail_list=[]
        total_score=0
        for user_score in mytestscore:

            detail_dict = {}
            user_score_score = user_score.score
            
            user_score_question_id = user_score.question_id
            test_taken = Question.objects.get(id=user_score_question_id)
            test_taken_name = test_taken.test

            question_attempts=test_taken.question

            detail_dict = {'test' : test_taken_name , 'question' : question_attempts , 'score' : user_score_score }

            test_taken_detail_list.append(detail_dict)
            total_score = total_score + user_score_score

        return render(request, 'my_score_list.html', {'test_taken':test_taken_detail_list , 'total_score':total_score})
    else:
         return HttpResponseRedirect('/login/?next=%s' % request.path)


#def question_to_test(request,question_id):








def allow_test(request ,test_id):
    
    if request.user.is_authenticated():

        
        username = User.objects.get(id=request.user.id)

        test_status=Test_Score.objects.filter(user=username , test_id=test_id).count()
        
        if test_status == 0:
         
            return True
        else:
            return False
    else:

        return HttpResponseRedirect('/login/')





















































def show_score(request ,test ,score ):  # not to use this one
    if request.user.is_authenticated():
        users = request.user.test_score
    username = Test_Score.objects.filter(users=users)
    return render(request, 'myscore.html', {'my_test_score': test_score})
